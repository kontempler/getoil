var express = require('express');
var request = require('request-promise')
var router = express.Router();
const googleApiKey = "AIzaSyA6TfU84r6wT2gu1NYAOCN7JkO342K21So";
var https = require('https');
const addressKeys = {
  "administrative_area_level_2": "estado",
  "administrative_area_level_1": "cidade",
  "sublocality_level_1": "bairro",
  "route": "logradouro",
  "street_number": "numero",
  "postal_code": "cep" 
}
const defaultTypes = 'gas_station';
const defaultRadius = 1000;

// GET Locais
async function getLatLng(lat, lng) {
  return await request(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&sensor=false&key=${googleApiKey}`)
}

// GET Locais
async function getLocal(lat, lng) {
  return await request(`https://maps.googleapis.com/maps/api/place/search/json?location=${lat},${lng}&radius=${defaultRadius}&type=${defaultTypes}&key=${googleApiKey}`)
}

router.post('/', function (req, res) {
  let reqLatLng = getLatLng(req.body.lat, req.body.lng);
  let reqLocal = getLocal(req.body.lat, req.body.lng);

  Promise.all([reqLatLng, reqLocal]).then((results) => {
    let parsedLatLng = JSON.parse(results[0])
    let parsedLocations = JSON.parse(results[1])
    
    let address = filterLatLng(parsedLatLng.results[0])
    let locations = filterLocations(parsedLocations.results)

    res.send(Object.assign(address, { postos: locations }));
  })
  .catch((error) => {
    res.status(error.statusCode).send("Algo deu errado :(")
  })

});

module.exports = router;

function filterLatLng(raw) {
  var resultAddressInfo = {}

  raw.address_components.filter(filterAddressComponents).map(component => {
    component.types.forEach(element => {
      if (Object.keys(addressKeys).includes(element)) {
        resultAddressInfo[addressKeys[element]] = component.long_name
      }
    });
  })
  return resultAddressInfo
}

function filterAddressComponents(value) {
  return Object.keys(addressKeys).some(r => value.types.includes(r))
}

function filterLocations(raw) {
  return raw.filter(filterOpened).map(location => {
    return {
      lat: location.geometry.location.lat,
      lng: location.geometry.location.lng,
      nome: location.name,
      endereco: location.vicinity
    }
  })
}

function filterOpened(value) {
  return value && value.opening_hours && value.opening_hours.open_now 
}

